// reactive DOM with JSON


let posts = [];
let count = 1;

document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	//prevents the page from reloading after a button are clicked/triggered
	e.preventDefault();
	// adds the information from the frontend to our mock database
	posts.push({
		id:count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	count++;

	showPosts(posts);
	alert('Successfully Added');
	console.log(posts);
});

const showPosts = (posts) => { 
	let postEntries = '';
	//
	posts.forEach((post)=>{

		postEntries += `<div id = "post-${post.id}">

		<h3 id="post-title-${post.id}">${post.title}</h3>

		<p id="post-body-${post.id}">${post.body}</p>

		<button onclick="editPost(${post.id})">Edit </button>

		<button onclick="deletePost(${post.id})">Delete</button>

		</div>`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-title-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
	e.preventDefault();
	for (let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-title-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			showPosts(posts);
			alert("Success!");
			break;
		}
	}
});

// function for deleting a specific post that updates it's array

const deletePost = (id) => {
	posts.splice(id-1,3);
	document.querySelector(`#post-${id}`).remove();
	count = 0;
}	